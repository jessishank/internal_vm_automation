# internal_vm_automation Cookbook
 
Baseline automation for vm's provisioned with terraform.   
Updates apt.
Installs and configures ntp. 
Creates `prodeng` group, adds users with passwordless sudo and ssh keys. 
Installs Prometheus's `node_exporter`, runs as a service. 

## Requirements

TODO: List your cookbook requirements. Be sure to include any requirements this cookbook has on platforms, libraries, other cookbooks, packages, operating systems, etc.

e.g.
### Platforms

- Ubuntu 

### Chef

- Chef 12.0 or later

### Cookbooks

- `toaster` - internal_vm_automation needs toaster to brown your bagel.

## Attributes

TODO: List your cookbook attributes here.

e.g.
### internal_vm_automation::default

<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['internal_vm_automation']['bacon']</tt></td>
    <td>Boolean</td>
    <td>whether to include bacon</td>
    <td><tt>true</tt></td>
  </tr>
</table>

## Usage

### internal_vm_automation::default

TODO: Write usage instructions for each cookbook.

e.g.
Just include `internal_vm_automation` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[internal_vm_automation]"
  ]
}
```


## License and Authors

Authors: Jessi Shank

