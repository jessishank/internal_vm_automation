default['internal_vm_automation']['release_number'] = '0.14.0'
default['internal_vm_automation']['distro'] = 'linux-amd64'
default['internal_vm_automation']['tag_names'] = [ 'level11-provisoned' ]


default['authorization']['sudo']['include_sudoers_d'] = true
