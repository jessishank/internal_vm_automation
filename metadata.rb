name             'internal_vm_automation'
maintainer       'Level 11 Consulting LLC'
maintainer_email 'jessishank@leve11.com'
license          'All rights reserved'
description      'Cookbook that sets up internal VMs'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.2.6'


depends 'ssh_authorized_keys'
depends 'sudo'
depends 'users'
depends 'apt'
depends 'ntp'
depends 'git'
depends 'docker-grid'
