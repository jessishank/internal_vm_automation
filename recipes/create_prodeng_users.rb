
#
# Cookbook Name:: internal_vm_automation
# Recipe:: create_prodeng_users
#
# Copyright 2017, Level 11 
#
# All rights reserved - Do Not Redistribute
# 
# Create group "prodeng", add users to it. configure that group to have passwordless sudo. 
#


group 'prodeng' do
  action :create
end

users_manage 'prodeng'  do
  group_id 3000
  action [:create]
  # todo: should be a subset
  data_bag 'users'
end

sudo 'prodeng' do 
  group '%prodeng'
  nopasswd true
  setenv true
end
