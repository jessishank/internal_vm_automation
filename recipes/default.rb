#
# Cookbook Name:: internal_vm_automation
# Recipe:: default
#
# Copyright 2017, Level 11 
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'apt::default'
include_recipe 'ntp::default'
include_recipe 'git'
# include_recipe 'docker-grid::engine'
# include_recipe 'docker-grid::compose'
include_recipe 'internal_vm_automation::create_prodeng_users'
include_recipe 'internal_vm_automation::node_exporter_setup'
include_recipe 'internal_vm_automation::tag_nodes'
