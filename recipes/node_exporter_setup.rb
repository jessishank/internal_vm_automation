#
# Cookbook Name:: internal_vm_automation
# Recipe:: node_exporter_setup
#
# Copyright 2017, Level 11 
#
# All rights reserved - Do Not Redistribute
# 
# Download node_exporter and setup as a service.  
#


# todo should be its own cookbook. 

remote_file '/opt/node_exporter.tar.gz' do
  source "https://github.com/prometheus/node_exporter/releases/download/v#{node['internal_vm_automation']['release_number']}/node_exporter-#{node['internal_vm_automation']['release_number']}.#{node['internal_vm_automation']['distro']}.tar.gz"
  owner 'root'
  mode '0755'
  action :create
  not_if { File.exists?("/opt/node_exporter.tar.gz")}
end


execute 'extract_node_exporter' do
  command 'sudo tar -xzvf /opt/node_exporter.tar.gz'
  cwd '/opt/'
  not_if { File.exists?("/opt/node_exporter/node_exporter")}
end


link 'node_exporter' do
  target_file "/usr/bin/node_exporter"
  to "/opt/node_exporter-#{node['internal_vm_automation']['release_number']}.#{node['internal_vm_automation']['distro']}/node_exporter"
end

template '/etc/systemd/system/node_exporter.service' do
  mode '0755'
  source 'node_exporter_service.erb'
end


service 'node_exporter' do
  supports :status => true, :restart => true, :reload => true
  action [ :enable, :start ]
end
