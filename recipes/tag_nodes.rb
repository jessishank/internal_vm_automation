
#
# Cookbook Name:: internal_vm_automation
# Recipe:: tag_nodes
#
# Copyright 2017, Level 11 
#
# All rights reserved - Do Not Redistribute
# 
# Tag node from node attribute node['internal_vm_automation']['tag_names'] array.
#


node['internal_vm_automation']['tag_names'].each do |tag_name|
  tag(tag_name)
end
